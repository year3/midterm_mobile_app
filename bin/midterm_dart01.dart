import 'dart:io';
import 'dart:math';

//Exercise 1: Tokenizing a String
Tokenizing(var math_expressions) {
  List<String> list_of_tokens = [];
  //parenthesis , an operator , a number
  //no charactor

  list_of_tokens = math_expressions.split(' ');
  for (int i = 0; i < list_of_tokens.length; i++) {
    //remove whitespace
    list_of_tokens.remove(' ');
  }
  return list_of_tokens;
}

//Exercise 2 : infix to Postfix
InfixToPostfix(var listoftokens) {
  List operators = [];
  List<String> postfix = [];
  int precedence, last_precedence = 10;

  for (int i = 0; i < listoftokens.length; i++) {
    if (listoftokens[i] == "0" ||
        listoftokens[i] == "1" ||
        listoftokens[i] == "2" ||
        listoftokens[i] == "3" ||
        listoftokens[i] == "4" ||
        listoftokens[i] == "5" ||
        listoftokens[i] == "6" ||
        listoftokens[i] == "7" ||
        listoftokens[i] == "8" ||
        listoftokens[i] == "9") {
      postfix.add(listoftokens[i]);
    }
    if (listoftokens[i] == "+" ||
        listoftokens[i] == "-" ||
        listoftokens[i] == "*" ||
        listoftokens[i] == "/" ||
        listoftokens[i] == "**" ||
        listoftokens[i] == "%") {
      if (listoftokens[i] == "(" || listoftokens[i] == ")") {
        precedence = 0;
      } else if (listoftokens[i] == "+" || listoftokens[i] == "-") {
        precedence = 1;
      } else if (listoftokens[i] == "*" || listoftokens[i] == "/") {
        precedence = 2;
      } else {
        precedence = 3;
      }
      if (operators.length > 0) {
        if (operators.last == "(" || operators.last == ")") {
          last_precedence = 0;
        } else if (operators.last == "+" || operators.last == "-") {
          last_precedence = 1;
        } else if (operators.last == "*" || operators.last == "/") {
          last_precedence = 2;
        } else {
          last_precedence = 3;
        }
      }
      while (operators.isNotEmpty &&
          operators.last != "(" &&
          precedence <= last_precedence) {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.add(listoftokens[i]);
    }
    if (listoftokens[i] == "(") {
      operators.add(listoftokens[i]);
    }
    if (listoftokens[i] == ")") {
      while (operators.last != "(") {
        String keep = operators.last;
        operators.removeLast();
        postfix.add(keep);
      }
      operators.remove("(");
    }
  }
  while (operators.isNotEmpty) {
    String keep = operators.last;
    operators.removeLast();
    postfix.add(keep);
  }
  return postfix;
}

evaluatePostFix(List<String> postfix) {
  List<double> val = [];
  double digit;
  for (int i = 0; i < postfix.length; i++) {
    if (postfix[i] == "0" ||
        postfix[i] == "1" ||
        postfix[i] == "2" ||
        postfix[i] == "3" ||
        postfix[i] == "4" ||
        postfix[i] == "5" ||
        postfix[i] == "6" ||
        postfix[i] == "7" ||
        postfix[i] == "8" ||
        postfix[i] == "9") {
      switch (postfix[i]) {
        case "0":
          digit = 0;
          val.add(digit);
          break;
        case "1":
          digit = 1;
          val.add(digit);
          break;
        case "2":
          digit = 2;
          val.add(digit);
          break;
        case "3":
          digit = 3;
          val.add(digit);
          break;
        case "4":
          digit = 4;
          val.add(digit);
          break;
        case "5":
          digit = 5;
          val.add(digit);
          break;
        case "6":
          digit = 6;
          val.add(digit);
          break;
        case "7":
          digit = 7;
          val.add(digit);
          break;
        case "8":
          digit = 8;
          val.add(digit);
          break;
        case "9":
          digit = 9;
          val.add(digit);
          break;
        default:
          break;
      }
    } else {
      double right;
      double left;
      right = val.last;
      val.removeLast();
      left = val.last;
      val.removeLast();
      double summation = 0;
      if (postfix[i] == "+") {
        summation = (left + right);
      }
      if (postfix[i] == "-") {
        summation = (left - right);
      }
      if (postfix[i] == "*") {
        summation = (left * right);
      }
      if (postfix[i] == "/") {
        summation = (left / right);
      }
      if (postfix[i] == "**") {
        summation = (pow(left, right) + 0.0);
      }
      if (postfix[i] == "%") {
        summation = (left % right);
      }
      val.add(summation);
    }
  }
  return val[0];
}

void main(List<String> arguments) {
  //5 * ( 2 + 3 ) - 4
  print("Enter Math Expressions: ");
  //input Math Expressions
  String? math_expressions = stdin.readLineSync();
  //tokenizing of math expressions
  List<String> listoftokens = Tokenizing(math_expressions);
  print("Token of Math Expressions is: ");
  print(listoftokens); //Output Exercise 1: Tokenizing a String
  List<String> postfix = InfixToPostfix(listoftokens);
  print(postfix); //output Exercise 2:Infix to Postfix
  double sum = evaluatePostFix(postfix);
  print(sum); //output Exercise 3:Evaluate Postfix
}
